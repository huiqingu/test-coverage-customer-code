package com.hexun.base;


/**
 * redis 配置
 * @author lining
 *
 */
public class RedissonConfig  {
	private String mRedisServer="10.0.200.59:6325,10.0.200.59:6326,10.0.200.59:6327";
	private String mRedisMaster="test_redis";
	private int mRedisPoolSize = 30;
	private int mRedisMinIdle = 10;
	private int mIdleConnectionTimeout = 10000;
	private int mTimeout = 5000;
	private int mConnectTimeout = 10000;
	/**
	 * 构造函数
	 */
	public  RedissonConfig() {
		
	}

	/**
	 * @return the redisServer
	 */
	public String getRedisServer() {
		return mRedisServer;
	}
	/**
	 * @param aRedisServer the redisServer to set
	 */
	public void setRedisServer(String aRedisServer) {
		mRedisServer = aRedisServer;
	}
	/**
	 * @return the redisMaster
	 */
	public String getRedisMaster() {
		return mRedisMaster;
	}
	/**
	 * @param aRedisMaster the redisMaster to set
	 */
	public void setRedisMaster(String aRedisMaster) {
		mRedisMaster = aRedisMaster;
	}
	/**
	 * @return the redisPoolSize
	 */
	public int getRedisPoolSize() {
		return mRedisPoolSize;
	}
	/**
	 * @param aRedisPoolSize the redisPoolSize to set
	 */
	public void setRedisPoolSize(int aRedisPoolSize) {
		mRedisPoolSize = aRedisPoolSize;
	}
	/**
	 * @return the redisMinIdle
	 */
	public int getRedisMinIdle() {
		return mRedisMinIdle;
	}
	/**
	 * @param aRedisMinIdle the redisMinIdle to set
	 */
	public void setRedisMinIdle(int aRedisMinIdle) {
		mRedisMinIdle = aRedisMinIdle;
	}

    /**
     * @return the idleConnectionTimeout
     */
    public int getIdleConnectionTimeout() {
        return mIdleConnectionTimeout;
    }

    /**
     * @param aIdleConnectionTimeout the idleConnectionTimeout to set
     */
    public void setIdleConnectionTimeout(int aIdleConnectionTimeout) {
        mIdleConnectionTimeout = aIdleConnectionTimeout;
    }

    /**
     * @return the timeout
     */
    public int getTimeout() {
        return mTimeout;
    }

    /**
     * @param aTimeout the timeout to set
     */
    public void setTimeout(int aTimeout) {
        mTimeout = aTimeout;
    }

    /**
     * @return the connectTimeout
     */
    public int getConnectTimeout() {
        return mConnectTimeout;
    }

    /**
     * @param aConnectTimeout the connectTimeout to set
     */
    public void setConnectTimeout(int aConnectTimeout) {
        mConnectTimeout = aConnectTimeout;
    }

	

}
