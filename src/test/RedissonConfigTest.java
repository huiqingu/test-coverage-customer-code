package com.hexun.test.utils;

import static org.junit.Assert.*;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hexun.base.RedissonConfig;

import net.sf.json.JSONObject;



public class TestRedissonConfig {
	private final static String S_CONTENT = "	{\n" + 
			"  \"redisLockTimeout\": 60,\n" + 
			"  \"redisMinIdle\": 10,\n" + 
			"  \"redisMaster\": \"test_redis\",\n" + 
			"  \"redisPoolSize\": 30,\n" + 
			"  \"redisServer\": \"redis://10.4.170.106:6380,redis://10.4.170.106:6381,redis://10.4.170.106:6382,redis://10.4.170.107:6380,redis://10.4.170.107:6381,redis://10.4.170.107:6382\",\n" + 
			"  \"idleConnectionTimeout\":40000,\n" + 
			"  \"timeout\":20000,\n" + 
			"  \"connectionTimeout\":10000\n" + 
			"}";
	private RedissonConfig mCfg ;
	@Before
	public void setUp() throws Exception {
		mCfg = new RedissonConfig();
		JSONObject jbo = JSONObject.fromObject(S_CONTENT);
		mCfg =(RedissonConfig) JSONObject.toBean(jbo, RedissonConfig.class);
		
		
	}

	@After
	public void tearDown() throws Exception {
		
	}

	@Test
	public void testGetRedisServer() {
		assertTrue("redis://10.4.170.106:6380,redis://10.4.170.106:6381,redis://10.4.170.106:6382,redis://10.4.170.107:6380,redis://10.4.170.107:6381,redis://10.4.170.107:6382".equals(mCfg.getRedisServer()));
	}

	@Test
	public void testGetRedisMaster() {
		assertTrue("test_redis".equals(mCfg.getRedisMaster()));
	}

	@Test
	public void testGetRedisPoolSize() {
		assertEquals(30, mCfg.getRedisPoolSize());
	}

	@Test
	public void testGetRedisMinIdle() {
		assertEquals(10, mCfg.getRedisMinIdle());
	}

	
	@Test
	public void testIdleConnectionTimeout() {
		assertEquals(40000, mCfg.getIdleConnectionTimeout());
	}
	@Test
	public void testTimeOut() {
		assertEquals(20000, mCfg.getTimeout());
	}
	@Test
	public void testConnectionTimeout() {
		assertEquals(10000, mCfg.getConnectTimeout());
	}
}
